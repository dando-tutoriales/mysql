-- ----------------------------------------------------------------
-- Episodio 8 : Mostrando informacion de diferentes tablas en MySQL
-- ----------------------------------------------------------------
-- insertar tema
USE trivia;
SET @nombreTema = 'Geografia';
DELETE FROM tema WHERE nombre = @nombreTema;
INSERT INTO tema (nombre) VALUES (@nombreTema);

-- insertar pregunta 1
USE trivia;
SET @idTema = (SELECT idTema FROM tema WHERE nombre = @nombreTema);
SELECT @idTema; 
SET @textoPregunta1 = '¿Cual es la capital de Honduras?';
INSERT INTO pregunta (texto, idTema) VALUES (@textoPregunta1, @idTema);
SET @idPregunta1 = (SELECT idPregunta FROM pregunta WHERE texto = @textoPregunta1);

-- insertar respuestas pregunta 1
USE trivia;
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('Tegucigalpa', @idPregunta1, false);
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('San Pedro Sula', @idPregunta1, false);
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('La Ceiba', @idPregunta1, true);

-- insertar pregunta 2
USE trivia;
SET @textoPregunta2 = '¿Cual es la capital de Colombia?';
INSERT INTO pregunta (texto, idTema) VALUES (@textoPregunta2, @idTema);
SET @idPregunta2 = (SELECT idPregunta FROM pregunta WHERE texto = @textoPregunta2);

-- insertar respuestas pregunta 2
USE trivia;
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('Buenos Aires', @idPregunta2, false);
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('Bogota', @idPregunta2, true);
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('Cali', @idPregunta2, false);
INSERT INTO respuesta (texto, idPregunta, esCorrecta) VALUES('Madrid', @idPregunta2, false);
