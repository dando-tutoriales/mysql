-- ----------------------------------------------------------------
-- Episodio 8 : Mostrando informacion de diferentes tablas en MySQL
-- ----------------------------------------------------------------

-- mostrar todas las preguntas (INNER JOIN)
USE trivia;
SELECT * FROM pregunta;
USE TRIVIA;
SELECT t.nombre AS Tema, p.texto AS Pregunta FROM pregunta p INNER JOIN tema t ON t.idTema = p.idTema;

-- LEFT JOIN: todos los registros de la izquierda y los que concuerden de la table en la derecha
SELECT 
    t.nombre AS Tema, 
    p.texto AS Pregunta
FROM
    pregunta p
LEFT JOIN
	tema t
ON
    t.idTema = p.idTema;

-- RIGHT JOIN: todos los registros de la derecha y los que concuerden de la table en la izquierda
SELECT 
    t.nombre AS Tema, 
    p.texto AS Pregunta
FROM
    pregunta p
RIGHT JOIN
	tema t
ON
    t.idTema = p.idTema;