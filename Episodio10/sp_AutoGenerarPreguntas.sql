DELIMITER //
USE trivia//
DROP PROCEDURE IF EXISTS AutoGenerarPreguntas//

CREATE PROCEDURE AutoGenerarPreguntas(
	IN TotalTemas INT,
    IN TotalPreguntasPorTema INT, 
    IN TotalRespuestasPorPregunta INT,
    IN BorrarRegistrosExistentes TINYINT
)
BEGIN
	DECLARE jugador1 VARCHAR(100) DEFAULT 'Henry';
    DECLARE jugador2 VARCHAR(100) DEFAULT 'Josue';
    DECLARE temasCreados INT DEFAULT 0;
    DECLARE nombreDelTema VARCHAR(100);
    DECLARE idTema INT DEFAULT 0;
    DECLARE idPregunta INT DEFAULT 0;
    DECLARE idRespuesta INT DEFAULT 0;
    DECLARE preguntasCreadas INT DEFAULT 0;
    DECLARE respuestasCreadas INT DEFAULT 0;    
    DECLARE textoDePregunta VARCHAR(200);
    DECLARE textoDeRespuesta VARCHAR(200);
    DECLARE esCorrecta TINYINT DEFAULT 0;
    
    IF(BorrarRegistrosExistentes) THEN
    BEGIN
		-- El WHERE es necesario en caso que MySQL este configurado en SAFE MODE
        DELETE FROM juego WHERE idTema > 0;
        DELETE FROM tema WHERE nombre IS NOT NULL;
        
    END;
    END IF;
    
    -- tema1
    
    WHILE temasCreados < TotalTemas  DO
		SET nombreDelTema = CONCAT('Tema ', temasCreados  + 1);
        INSERT INTO tema (nombre) VALUES (nombreDelTema);
        
        SET temasCreados = temasCreados + 1;
        
        SET idTema = last_insert_id();
        -- preguntas
        WHILE preguntasCreadas < TotalPreguntasPorTema DO
			SET textoDePregunta = CONCAT('Pregunta ', (preguntasCreadas + (temasCreados * 100) + 1 ));
            INSERT INTO pregunta (idTema, texto) VALUES (idTema, textoDePregunta);
            SET preguntasCreadas = preguntasCreadas + 1;
            
            SET idPregunta = last_insert_id();
            -- respuestas
            WHILE respuestasCreadas < TotalRespuestasPorPregunta DO
				SET textoDeRespuesta = CONCAT('Respuesta ', ((temasCreados * 100) + (respuestasCreadas) + 1));
                SET esCorrecta = IF(respuestasCreadas = 0, 1, 0);
                INSERT INTO respuesta (idPregunta, texto, EsCorrecta) VALUES (idPregunta, textoDeRespuesta, esCorrecta);                
                SET respuestasCreadas = respuestasCreadas + 1;
                SET idRespuesta = last_insert_id();
                
                -- juego: jugador 1
                INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador1, idTema, idPregunta, idRespuesta, now());
                INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador1, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-1, NOW()));
				IF(esCorrecta) THEN
                BEGIN
					INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador1, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-2, NOW()));
                    INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador1, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-3, NOW()));
                    INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador1, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-4, NOW()));
                END;
                END IF;
				-- juego: jugador 2
                INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador2, idTema, idPregunta, idRespuesta, now());
                INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador2, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-1, NOW()));
				IF(esCorrecta) THEN
                BEGIN
					INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador2, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-2, NOW()));
                    INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador2, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-3, NOW()));
                    INSERT INTO juego (jugador, idTema, idPregunta, idRespuesta, Fecha) VALUES(jugador2, idTema, idPregunta, idRespuesta, TIMESTAMPADD(MONTH,-4, NOW()));
                END;
                END IF;
            END WHILE; -- respuestas
            SET respuestasCreadas = 0;
		END WHILE; -- preguntas  
        SET preguntasCreadas = 0;        
    END WHILE; -- temas
    
END //
DELIMITER ;

