USE trivia;
DROP TABLE IF EXISTS Juego;
CREATE TABLE juego (
	IdJuego INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
	IdTema INT,
    IdPregunta INT,
    IdRespuesta INT,
    Jugador VARCHAR(300),
    Fecha DATETIME,
    CONSTRAINT `fk_juego_idtema` FOREIGN KEY (`idTema`) REFERENCES `tema` (`idTema`) ON DELETE CASCADE,
    CONSTRAINT `fk_juego_idpregunta` FOREIGN KEY (`idPregunta`) REFERENCES `pregunta` (`idPregunta`) ON DELETE CASCADE,
    CONSTRAINT `fk_juego_idrespuesta` FOREIGN KEY (`idRespuesta`) REFERENCES `respuesta` (`idRespuesta`) ON DELETE CASCADE
);
TRUNCATE TABLE juego;