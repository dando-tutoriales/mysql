-- total de registros en la table preguntas

USE trivia;
SET @nombreDelTema = 'Geografia';
SELECT 
	COUNT(p.idPregunta) AS TOTAL_PREGUNTAS 
FROM 
	pregunta AS p
		INNER JOIN
	tema AS t ON p.idTema = t.idTema
WHERE
	t.nombre = @nombreDelTema;