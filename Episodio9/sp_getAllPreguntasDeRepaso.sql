-- Store procedure usando parametro de entrada IN
USE trivia;
DROP procedure IF EXISTS getAllPreguntasDeRepaso;

DELIMITER //
CREATE PROCEDURE `getAllPreguntasDeRepaso` (IN nombreDelTema VARCHAR(200))
BEGIN
	SELECT 
		t.nombre AS 'Tema',
		p.texto AS 'Pregunta',
		r.texto AS 'Respuesta'
	FROM
		pregunta AS p
			INNER JOIN
		tema AS t ON p.idTema = t.idTema
			INNER JOIN
		respuesta AS r ON r.idPregunta = p.idPregunta
	WHERE
		r.EsCorrecta = 1 AND t.nombre = nombreDelTema
	ORDER BY t.nombre;
END //
DELIMITER ;

-- Para ejecutar el nuevo store procedure
-- CALL trivia.getAllPreguntasDeRepaso('geografia');
