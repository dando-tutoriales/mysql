DROP PROCEDURE IF EXISTS VerPrecioDelProducto;
DELIMITER //
CREATE PROCEDURE VerPrecioDelProducto(
	IN Costo decimal(4),
    OUT Precio decimal(4)
)
BEGIN
	SELECT (Costo + (Costo * 0.5)) INTO Precio;
END //
DELIMITER ;
