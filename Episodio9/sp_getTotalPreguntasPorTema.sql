-- Store procedure usando parametro de entrada IN y de salida OUT

USE trivia;
DROP procedure IF EXISTS GetTotalDePreguntasPorTema;

DELIMITER //
CREATE PROCEDURE GetTotalDePreguntasPorTema(
	IN nombreDelTema VARCHAR(200),
    OUT total INT
)
BEGIN	
	SET @nombreDelTema = 'Geografia';
	SELECT 
		COUNT(p.idPregunta)
        INTO total
	FROM 
		pregunta AS p
			INNER JOIN
		tema AS t ON p.idTema = t.idTema
	WHERE
		t.nombre = @nombreDelTema;
END //
DELIMITER ;


CALL trivia.GetTotalDePreguntasPorTema('Geografia', @total); 
SELECT @total;