USE trivia;
SET @tema = 'geografia';
SELECT 
    t.nombre AS 'Tema',
    p.texto AS 'Pregunta',
    r.texto AS 'Respuesta'
FROM
    pregunta AS p
        INNER JOIN
    tema AS t ON p.idTema = t.idTema
        INNER JOIN
    respuesta AS r ON r.idPregunta = p.idPregunta
WHERE
    r.EsCorrecta = 1  AND
    t.nombre = @tema
ORDER BY t.nombre , p.texto;