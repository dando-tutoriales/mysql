-- CREAR TABLA: tema
CREATE TABLE `tema` (
  `idTema` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`idTema`),
  UNIQUE KEY `name_UNIQUE` (`name`)
);

-- CREAR TABLA: pregunta
CREATE TABLE `trivia`.`pregunta` (
  `idPregunta` INT NOT NULL AUTO_INCREMENT,
  `texto` VARCHAR(300) NOT NULL,
  `idTema` INT NOT NULL,
  PRIMARY KEY (`idPregunta`),
  UNIQUE INDEX `texto_UNIQUE` (`texto` ASC) VISIBLE,
  INDEX `fk_idTema_idx` (`idTema` ASC) VISIBLE,
  CONSTRAINT `fk_idTema`
    FOREIGN KEY (`idTema`)
    REFERENCES `trivia`.`tema` (`idTema`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);

-- CREAR TABLA: respuesta
CREATE TABLE `trivia`.`respuesta` (
  `idRespuesta` INT NOT NULL AUTO_INCREMENT,
  `texto` VARCHAR(300) NOT NULL,
  `idPregunta` INT NOT NULL,
  `isCorrect` TINYINT,
  PRIMARY KEY (`idRespuesta`),
  UNIQUE INDEX `texto_UNIQUE` (`texto` ASC) VISIBLE,
  INDEX `fk_idPregunta_idx` (`idPregunta` ASC) VISIBLE,
  CONSTRAINT `fk_idPregunta`
    FOREIGN KEY (`idPregunta`)
    REFERENCES `trivia`.`pregunta` (`idPregunta`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);